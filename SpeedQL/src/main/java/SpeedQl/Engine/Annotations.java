package SpeedQl.Engine;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

public class Annotations {

	@Retention(RUNTIME)
	@Target(ElementType.FIELD)
	public static @interface Id {
	}

	@Retention(RUNTIME)
	@Target(ElementType.TYPE)
	public static @interface Table {
		String name();
	}

	@Retention(RUNTIME)
	@Target(FIELD)
	public static @interface Join {
		String target_collumn();

		String base_collumn();
	}

	@Retention(RUNTIME)
	@Target(FIELD)
	public static @interface Joins {
		Join[] JOINS();
	}
	
	@Retention(RUNTIME)
	@Target(FIELD)
	public static @interface SqlType {
		String type();
	}
	
	@Retention(RUNTIME)
	@Target(FIELD)
	public static @interface Foreign {
		Class<?> table();
		String field();
	}
	
}
