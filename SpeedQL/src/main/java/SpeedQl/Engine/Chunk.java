package SpeedQl.Engine;

import java.util.List;

public class Chunk<T> {

	Predicate<T> predicate;
	private boolean hasNext = true;
	private int chunkSize;
	private int start = 0;

	protected Chunk(Predicate<T> predicate, int chunkSize) {
		this.predicate = predicate;
		this.chunkSize = chunkSize;
	}

	public boolean isNext() {
		return hasNext;
	}

	public List<T> getNext() {
		List<T> result = predicate.limit(start, chunkSize).get();
		hasNext = result.size() == chunkSize;
		start += chunkSize;
		return result;
	}

}
