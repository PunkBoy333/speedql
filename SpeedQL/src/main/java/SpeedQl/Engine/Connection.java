package SpeedQl.Engine;

import java.time.LocalDateTime;

class Connection {

	java.sql.Connection connection;
	int connection_id;
	String target;
	String db;
	boolean bussi;
	LocalDateTime lastGet = null;

	public java.sql.Connection getConnection() {
		return connection;
	}

	public void setConnection(java.sql.Connection connection) {
		this.connection = connection;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public LocalDateTime getLastGet() {
		return lastGet;
	}

	public void setLastGet(LocalDateTime lastGet) {
		this.lastGet = lastGet;
	}
	
	

}
