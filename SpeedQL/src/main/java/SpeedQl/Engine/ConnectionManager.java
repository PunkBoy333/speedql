package SpeedQl.Engine;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ConnectionManager {

	private static Map<String, ConnectionManager> managers = new HashMap<String, ConnectionManager>();

	private List<Connection> pool;

	private Map<Properti, Object> properties;

	private Thread hendler;

	public static ConnectionManager inicialized(String named, Map<Properti, Object> properties) {
		ConnectionManager manager = new ConnectionManager();
		manager.properties = properties;
		if (!manager.properties.containsKey(Properti.POOL_MIN)) {
			manager.properties.put(Properti.POOL_MIN, 1);
		}
		if (!manager.properties.containsKey(Properti.POOL_MAX)) {
			manager.properties.put(Properti.POOL_MAX, 2);
		}
		if (!manager.properties.containsKey(Properti.TIMEOUT)) {
			manager.properties.put(Properti.TIMEOUT, 20);
		}
		manager.hendler(false);
		manager.hendler = new Thread(new Runnable() {
			@Override
			public void run() {
				manager.hendler(true);
			}
		});

		manager.hendler.start();
		ConnectionManager.managers.put(named, manager);
		return manager;
	}

	/**
	 * @param named connection named when null get first connection
	 */
	public static ConnectionManager getNamedConnection(String named) {
		if (named == null)
			named = managers.keySet().stream().findFirst().get();

		return ConnectionManager.managers.get(named);
	}

	private Connection createConnection() {
		Properties sql_config = new Properties();
		if (properties.containsKey(Properti.User))
			sql_config.put("user", properties.get(Properti.User));
		if (properties.containsKey(Properti.Passwd))
			sql_config.put("password", properties.get(Properti.Passwd));
		sql_config.put("serverTimezone", TimeZone.getDefault().getID());
		try {
			java.sql.Connection connection = DriverManager.getConnection(
					"jdbc:mysql://" + properties.get(Properti.Host) + "/" + properties.get(Properti.Database),
					sql_config);
			Connection conn = new Connection();
			conn.bussi = false;
			PreparedStatement st = connection.prepareStatement("SELECT  connection_id()");
			ResultSet rs = st.executeQuery();
			rs.next();
			conn.connection_id = rs.getInt(1);
			conn.connection = connection;
			conn.target = connection.getMetaData().getURL();
			conn.db = connection.getMetaData().getDatabaseProductName();
			conn.lastGet = LocalDateTime.now();
			rs.close();
			st.close();
			return conn;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	protected Connection getConnection() {
		if (this.pool == null)
			hendler(false);
		Connection cn = this.pool.parallelStream().filter(a -> {
			try {
				return !a.connection.isClosed() && !a.bussi;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}).findFirst().orElse(null);
		
		if (cn == null && this.pool.size() < (int) Integer.parseInt(properties.get(Properti.POOL_MAX).toString())) {
			Connection con = createConnection();
			con.bussi = true;
			this.pool.add(con);
			return con;
		} else if (cn != null) {
			cn.bussi = true;
			cn.lastGet = LocalDateTime.now();
			return cn;
		} else {
			try {
				System.out.println("sleep");
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return getConnection();
		}
	}

	protected void dropConnection(Connection conn) {
		conn.bussi = false;
	}

	public static List<Object[]> nativeQuerry(String connectionNamed, String query) throws Exception {
		List<Object[]> objects = new ArrayList<Object[]>();
		ConnectionManager connect = getNamedConnection(connectionNamed);
		Connection con = connect.getConnection();
		Statement st = con.getConnection().createStatement();
		ResultSet rs = st.executeQuery(query);
		java.sql.ResultSetMetaData meta = rs.getMetaData();
		while (rs.next()) {

			Object[] ob = new Object[meta.getColumnCount()];
			for (int i = 1; i <= meta.getColumnCount(); i++) {
				ob[i - 1] = rs.getObject(i);
			}
			objects.add(ob);
		}
		rs.close();
		st.close();
		connect.dropConnection(con);
		return objects;
	}

	private void hendler(boolean thread) {
		int waitTime = Integer.parseInt(properties.get(Properti.TIMEOUT).toString());
		boolean run = !thread;
		while (run || thread) {

			try {
				if (pool == null) {
					pool = new ArrayList<Connection>();
				}
				Connection c = createConnection();
				PreparedStatement st = c.connection.prepareStatement(
						"SELECT * FROM INFORMATION_SCHEMA.PROCESSLIST WHERE COMMAND like 'sleep' and TIME > "
								+ properties.get(Properti.TIMEOUT));
				ResultSet rs = st.executeQuery();
				List<Integer> time_out_ids = new ArrayList<Integer>();
				while (rs.next()) {
					time_out_ids.add(rs.getInt(1));
				}
				rs.close();
				st.close();
				c.connection.close();
				List<Connection> conn = new ArrayList<Connection>();
				LocalDateTime now = LocalDateTime.now();
				pool.stream().filter(a -> time_out_ids.contains(a.connection_id) && a.lastGet.plusSeconds((int)properties.get(Properti.TIMEOUT)).isBefore(now) ).forEach(a -> {
					try {
						a.connection.close();
						// pool.remove(a);
						conn.add(a);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
				conn.stream().forEach(a -> {
					pool.remove(a);
				});
				// pool = pool.parallelStream().filter(a->time_out_ids.contains(a.connection_id
				// )).collect(Collectors.toList());
				for (int i = pool.size(); i < (int) Integer
						.parseInt(properties.get(Properti.POOL_MIN).toString()); i++) {
					pool.add(createConnection());
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			if (run) {
				run = false;
				break;
			}
			try {
				Thread.sleep(waitTime * 1000);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static enum Properti {
		/**
		 * server [ip addres or domain] [:] [port]
		 */
		Host,
		/**
		 * login user name
		 */
		User,
		/**
		 * login password
		 */
		Passwd,
		/**
		 * connected db name
		 */
		Database,
		/**
		 * avaliable connection min
		 */
		POOL_MIN,
		/**
		 * avaliable connection max
		 */
		POOL_MAX,
		/**
		 * sec : chak connection avaliable and bus
		 */
		TIMEOUT
	}

}
