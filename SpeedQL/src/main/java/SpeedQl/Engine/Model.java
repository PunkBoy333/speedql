package SpeedQl.Engine;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import SpeedQl.Engine.Annotations.Foreign;
import SpeedQl.Engine.Annotations.Id;
import SpeedQl.Engine.Annotations.Join;
import SpeedQl.Engine.Annotations.Joins;
import SpeedQl.Engine.Annotations.SqlType;
import SpeedQl.Engine.Annotations.Table;

public abstract class Model<T> {

	public Model() {
	}

	protected T origin;
	protected List<String> collumns;
	protected String tableIdName = "id";

	protected List<String> joined = new ArrayList<String>();
	/**
	 * Required return this type object not null
	 */
	public abstract T clone();

	public abstract String ConnectionNamed();

	@SuppressWarnings("unchecked")
	@Deprecated
	public Predicate<T> query() {
		return new Predicate<T>((Class<T>) getClass(), ConnectionNamed());
	}
	
	@SuppressWarnings("unchecked")
	public QuerryBuilder<T> createQuery() {
		return new QuerryBuilder<T>((Class<T>) getClass()).setConnetcionName(ConnectionNamed());
	}

	public List<String> idsName() {
		List<String> ls = getSqlFields(this.getClass() , false).stream()
				.filter(a -> a.getDeclaredAnnotation(Id.class) != null).map(mapper -> mapper.getName())
				.collect(Collectors.toList());
		if (ls.size() < 1)
			ls.add(tableIdName);

		return ls;
	}

	public String getTableId(String name) {

		try {
			Field f = this.getClass().getDeclaredField(name);
			f.setAccessible(true);
			String id = f.get(this.origin) + "";
			f.setAccessible(false);
			return id;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public Object get(Field f) {
		try {
			
			f.setAccessible(true);
			if (f.get(this) != null || this.joined.contains(f.getName()))
				return f.get(this);
			try {
				Class<?> m = f.getType();
				boolean is_array = false;
				if (m.getName().toLowerCase().contains("list") || m.getName().toLowerCase().contains("set")) {
					ParameterizedType pType = (ParameterizedType) f.getGenericType();
					m = (Class<?>) pType.getActualTypeArguments()[0];
					is_array = true;
				}
				QuerryBuilder<?> pr = ((Model<?>) m.newInstance()).createQuery();

				Field base = this.getClass().getDeclaredField(f.getDeclaredAnnotation(Join.class).base_collumn());
				base.setAccessible(true);
				Object value = null;
				List<?> list = pr.where(f.getDeclaredAnnotation(Join.class).target_collumn(), base.get(this)).get();
				if(is_array)
					value = list;
				else if(list != null && list.size()>0)
					value = list.get(0);

				f.set(this, value);
				base.setAccessible(false);
				f.setAccessible(false);
				this.joined.add(f.getName());
				return value;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}

		} catch (Exception e) {
			f.setAccessible(false);
			return null;
		}

	}

	public Object get(String f) {
		try {
			return get(this.getClass().getDeclaredField(f));
		} catch (Exception e) {
			return null;
		}
	}

	public int save() {
		List<Field> origin_fields = getSqlFields(this.getClass() , false);
		try {
			try {
				Field update = origin_fields.parallelStream().filter(a-> a.getName().equals("updated_at")).findFirst().orElse(null);
				if(update != null) {
					update.setAccessible(true);
					update.set(this, LocalDateTime.now());
					update.setAccessible(false);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			ConnectionManager manager = ConnectionManager.getNamedConnection(this.ConnectionNamed());
			Connection connection = manager.getConnection();
			connection.getConnection().setAutoCommit(false);
			Statement st = connection.connection.createStatement();
			try {
				if (origin == null) {
					try {
						Field c_at = origin_fields.parallelStream().filter(a-> a.getName().equals("created_at")).findFirst().orElse(null);
						if(c_at != null) {
							c_at.setAccessible(true);
							c_at.set(this, LocalDateTime.now());
						}
					}catch (Exception e) {
						// TODO: handle exception
					}
					
					
					List<Field> fields = origin_fields.parallelStream()
							.filter(a -> (a.getDeclaredAnnotation(SqlType.class) == null || !a
									.getDeclaredAnnotation(SqlType.class).type().toUpperCase().contains("AUTO_INCREMENT"))
									&& (collumns == null || collumns.contains(a.getName())) && a.getDeclaredAnnotation(Join.class) == null
									&& a.getDeclaredAnnotation(Joins.class) == null)
							.collect(Collectors.toList());
					
					String inset = "INSERT INTO " + this.getClass().getDeclaredAnnotation(Table.class).name()
							+ " ("
							+ String.join(",",
									fields.parallelStream().map(m -> "`" + m.getName() + "`").collect(Collectors.toList()))
							+ ")" + "VALUES (" + String.join(",", fields.parallelStream().map(a -> {
								a.setAccessible(true);
								Object o = null;
								try {
									o = a.get(this);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								return (o != null && a.getType().equals(String.class))
										? "'" + o.toString().replaceAll("'", "\\'") + "'"
										: (o != null && a.getType().equals(LocalDateTime.class))
												? "'" + o.toString().replaceAll("T", " ") + "'"
												: o + "";
							}).collect(Collectors.toList())) + ")";
					//System.out.println(inset);
					
					int i = st.executeUpdate(inset);
					st.close();
					connection.getConnection().commit();
					T t = this.query().orderBy(this.idsName().get(0), "desc").limit(0, 1).get().get(0);
					for (Field f : getSqlFields(this.getClass() , false)) {
						f.setAccessible(true);
						f.set(this, f.get(t));
						f.setAccessible(false);
					}
					manager.dropConnection(connection);
					return i;
				} else {
					List<Field> fields = getSqlFields(this.getClass() , false).parallelStream().filter(a -> {
	
						try {
							a.setAccessible(true);
							boolean b =(a.get(this) == null && a.get(this.origin) != null) || !a.get(this).equals(a.get(this.origin));
							a.setAccessible(false);
							return collumns.contains(a.getName()) && b;
						} catch (Exception e) {
							return false;
						}
					}).collect(Collectors.toList());
					if (fields.size() < 1)
						return 0;
					String qxeU = "UPDATE " + this.getClass().getDeclaredAnnotation(Table.class).name()
							+ " SET " + String.join(",", fields.parallelStream().map(a -> {
								a.setAccessible(true);
								Object o = null;
								try {
									o = a.get(this);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								return (o != null && a.getType().equals(String.class))
										? "`" + a.getName() + "`='" + o.toString().replaceAll("'", "\\'") + "'"
										: (o != null && a.getType().equals(LocalDateTime.class))
												? "`" + a.getName() + "`='" + o.toString().replaceAll("T", " ") + "'"
												: "`" + a.getName() + "`=" + o + "";
							}).collect(Collectors.toList())) + " WHERE " + String.join(" and ", this.idsName().stream()
									.map(m -> "`" + m + "`='" + this.getTableId(m) + "'").collect(Collectors.toList()));
					//System.out.println(qxeU);
					int update = st.executeUpdate(qxeU);
					st.close();
					connection.getConnection().commit();
					manager.dropConnection(connection);
					return update;
				}
			}catch (Exception e) {
				connection.getConnection().rollback();
				connection.getConnection().setAutoCommit(true);
				st.close();
				manager.dropConnection(connection);
				throw e;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public int update(Map<String, Object> valueUpdate) {
		if (this.origin == null)
			throw new RuntimeException("Not found in DB");
		List<Field> fields = this.getSqlFields(this.getClass(), false);
		valueUpdate.keySet().forEach(a -> {
			try {
				Field f = fields.parallelStream().filter(field-> field.getName().equals(a)).findFirst().orElse(null);
				f.setAccessible(true);
				f.set(this, valueUpdate.get(a));
				f.setAccessible(false);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});

		return save();
	}

	public boolean delete() {
		if (this.origin == null)
			throw new RuntimeException("Not found in DB");

		ConnectionManager manager = ConnectionManager.getNamedConnection(this.ConnectionNamed());
		Connection connection = manager.getConnection();
		try {
			Statement st = connection.connection.createStatement();
			int i = st.executeUpdate("DELETE FROM "+this.getClass().getDeclaredAnnotation(Table.class).name()+"\r\n" + "WHERE " + String.join(" and ", this.idsName()
					.stream().map(m -> "`" + m + "`='" + this.getTableId(m) + "'").collect(Collectors.toList())));
			st.close();
			return i > 0;
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			manager.dropConnection(connection);
		}
		return true;
	}

	public boolean softDelete() {
		Map<String, Object> objects = new HashMap<String, Object>();
		objects.put("deleted_at", LocalDateTime.now());
		return update(objects) > 0;
	}

	public boolean restore() {
		Map<String, Object> objects = new HashMap<String, Object>();
		objects.put("deleted_at", null);
		return update(objects) > 0;
	}

	public void createTable(String... columns)  throws Exception{
		ConnectionManager manager = ConnectionManager.getNamedConnection(this.ConnectionNamed());
		Connection connection = manager.getConnection();
		connection.getConnection().setAutoCommit(false);
		Statement st = connection.connection.createStatement();
		try {
			List<String> c_array = collumns == null ? null : Arrays.asList(columns);
			List<String> ids = new ArrayList<>(Arrays.asList(this.getClass().getDeclaredFields()).parallelStream()
					.filter(a -> a.getDeclaredAnnotation(Id.class) != null).map(a -> a.getName())
					.collect(Collectors.toList()));
			if (ids.size() < 1)
				ids.add(this.tableIdName);
			String sql = "CREATE TABLE " + this.getClass().getDeclaredAnnotation(Table.class).name() + "(\n"
					+ String.join(",\n", getSqlFields(this.getClass() , true).parallelStream()
							.filter(a -> (c_array == null
									|| c_array.contains(a.getName()) ) && a.getDeclaredAnnotation(SqlType.class) != null)
							.map(m ->{
								//Enums
							if(m.getType().isEnum()) {
								String enum_values = String.join(",",  Arrays.asList(m.getType().getEnumConstants())
									.stream()
									.map(a-> "'"+a+"'").collect(Collectors.toList()));
								
								return "`"+m.getName() + "` ENUM("+enum_values+") ";
							}else {
							return "`"+m.getName() + "` " + m.getDeclaredAnnotation(SqlType.class).type() +
									(m.getType().equals(String.class) && !m.getDeclaredAnnotation(SqlType.class).type().toUpperCase().contains("AUTO_INCREMENT") && m.getDeclaredAnnotation(SqlType.class).type().toUpperCase().contains("NOT NULL") ? " CHECK("+m.getName()+" > '') " : "");
							}
							})
							.collect(Collectors.toList()))
					+ ",\n PRIMARY KEY (" + String.join(",", ids) + ")\n);";
			//System.out.println(sql);
			st.executeUpdate(sql);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				st.close();
				connection.getConnection().commit();
			}catch (Exception e) {
				// TODO: handle exception
			}
			
			manager.dropConnection(connection);
		}
		
	}

	public void createForegenKey() throws Exception {
		ConnectionManager manager = ConnectionManager.getNamedConnection(this.ConnectionNamed());

		Connection connection = manager.getConnection();
		connection.getConnection().setAutoCommit(false);
		Statement st = connection.connection.createStatement();
		try {
			
			getSqlFields(this.getClass() , true).stream()
					.filter(a -> a.getDeclaredAnnotation(Foreign.class) != null).forEach(a -> {
						try {
							Foreign fk = a.getDeclaredAnnotation(Foreign.class);
							st.executeUpdate("ALTER TABLE " + this.getClass().getDeclaredAnnotation(Table.class).name()
									+ "\n" + "ADD FOREIGN KEY (" + a.getName() + ") REFERENCES "
									+ fk.table().getDeclaredAnnotation(Table.class).name() + "(" + fk.field() + ");");
						} catch (SQLException e) {
							Foreign fk = a.getDeclaredAnnotation(Foreign.class);
							e.printStackTrace();
							System.out.println("ALTER TABLE "
									+ this.getClass().getDeclaredAnnotation(Table.class).name() + "\n"
									+ "ADD FOREIGN KEY (" + a.getName() + ") REFERENCES "
									+ fk.table().getDeclaredAnnotation(Table.class).name() + "(" + fk.field() + ");");
						}
					});
		} catch (Exception e) {
			throw new RuntimeException(e);
		}finally {
			try {
				st.close();
				connection.getConnection().commit();
			}catch (Exception e) {
				// TODO: handle exception
			}
			
			manager.dropConnection(connection);
		}
	}
	
	@SuppressWarnings("unchecked")
	public T firstOrCreate(Map<String , Object> valueMap) throws Exception {
		QuerryBuilder<T> predict = this.createQuery();
		valueMap.keySet().stream().forEach(a->{
			predict.where(a , valueMap.get(a));
		});
		T element = predict.first();
		if(element == null) {
			final T finalelement = this.clone();
			valueMap.keySet().stream().forEach(a->{
				
				try {
					Field f = finalelement.getClass().getDeclaredField(a);
					f.setAccessible(true);
					f.set(finalelement, valueMap.get(a));
				} catch (Exception e) {
					throw new RuntimeException(e);
				} 
				
				
			});
			((Model<T>)finalelement).save();
			element = finalelement;
			
		}
		return element;
	}
	
	
	private List<Field> getSqlFields( Class<?> clazz , boolean is_create) {
		List<Field> fields = new ArrayList<Field>();
		
		for(Field f : clazz.getDeclaredFields()) {
			if(!is_create || f.getDeclaredAnnotation(SqlType.class) != null)
				fields.add(f);
		}
		if(clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Model.class)) {
			fields.addAll(this.getSqlFields(clazz.getSuperclass() , is_create));
		}
			
		
		return fields;
	}
}
