package SpeedQl.Engine;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.codec.digest.DigestUtils;

import SpeedQl.Engine.Annotations.Join;
import SpeedQl.Engine.Annotations.Joins;

public class Predicate<T> extends Object{

	String select = "*";
	String where = "";
	String tableHeader;
	Map<String, List<Field>> fieldMap;
	private Class<T> clazz;
	private String named;
	ConnectionManager manager;
	
	private long lastQuerryTime = 0;
	
	/*Return by millis*/
	public long getLastQuerryTime() {
		return lastQuerryTime;
	}

	protected Predicate(Class<T> class1 , String named) {
		this.clazz = class1;
		this.named = named;
		this.manager = ConnectionManager.getNamedConnection(named);
		tableHeader = "e";
		fieldMap = new HashMap<String, List<Field>>();
		fieldMap.put("e", new ArrayList<Field>(Arrays.asList(class1.getDeclaredFields())));
		fieldMap.get("e").addAll(new ArrayList<Field>(Arrays.asList(class1.getSuperclass().getDeclaredFields())));
		
	}
	
	String native_query = null;
	public List<T> getNativeQuery(String native_query) {
		List<T> result = new ArrayList<T>();
		this.native_query = native_query;
		try {
			result =  get();
		}catch (Exception e) {
			e.printStackTrace();
			return result = null;
		}finally {
			this.native_query = null;
		}
		return result;
		
	}

	@SuppressWarnings("unchecked")
	public List<T> get() {
		LocalDateTime start = LocalDateTime.now();
		List<T> tList = new ArrayList<T>();
		Connection con = manager.getConnection();
		try {
			T clone = (T) clazz.getConstructor().newInstance();
			
			java.sql.CallableStatement st = con.connection.prepareCall(this.native_query != null ? this.native_query : toSql());
			ResultSet rs = st.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			List<String> collumns = new ArrayList<String>();
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				collumns.add(rsmd.getColumnName(i));
			}
			//System.out.println("Exec query::" + ChronoUnit.SECONDS.between(start, LocalDateTime.now()));
			while (rs.next()) {
				T o = ((Model<T>) clone).clone();
				T origin = ((Model<T>) clone).clone();
				fieldMap.keySet().stream().forEach(a -> {
					fieldMap.get(a).stream()
					.filter(v-> collumns.contains(v.getName()))
					.forEach(b -> {
						try {
							if(collumns.contains(b.getName()))
								TypeCast.setCastValue(b, o, rs);
								b.setAccessible(true);
								b.set(origin, b.get(o));
						} catch (Exception e) {
							//System.out.println(b.getName());
							throw new RuntimeException(e);
						}
					});
				});
				((Model<T>) o).origin = origin;
				((Model<T>) o).collumns =collumns;
				tList.add(o);
			}
			//System.out.println("Build list::" + ChronoUnit.SECONDS.between(start, LocalDateTime.now()));
		st.close();
		rs.close();
		} catch (Exception e) {
			//System.out.println(this.toSql());
			throw new RuntimeException(e);
		}
		manager.dropConnection(con);
		if(tList == null || tList.size()<1) return tList;
		
		List<String> usedPrefix = new ArrayList<String>();
		this.withList.keySet().stream().sorted((a, b) -> a.compareTo(b)).forEach(a -> {

			String[] fields = a.split("\\.");
			Predicate<?> p = this;
			List<?> result = tList;
			String pre = "";
			for (String t : fields) {
				//System.out.println(pre + t);
				pre += "."+t;
				Field f = p.fieldMap.get("e").parallelStream().filter(r -> r.getName().equals(t)).findFirst().get();
				f.setAccessible(true);
				List<JOIN> joins = getJoins(f, p.clazz);
				JOIN.enable(joins);
				Class<?> type = f.getType();
				if (type.getName().toLowerCase().contains("list") || type.getName().toLowerCase().contains("set")) {
					ParameterizedType pType = (ParameterizedType) f.getGenericType();
					type = (Class<?>) pType.getActualTypeArguments()[0];
				}
				Predicate<?> p2 = new Predicate<>(type , this.named);
				if (this.withList.get(a) != null && a.endsWith(t)) {
					p2.where += this.withList.get(a).where;
				}
				for (JOIN j : joins) {

					Field remote = j.base;
					p2.whereIn(j.target.getName(), result.parallelStream().map(m -> {
						try {
							return remote.get(m);
						} catch (Exception e) {
							return "";
						}
					}).distinct());
				}
				List<Object> result2_uf = null;
				if (/*usedPrefix.contains(a.replaceAll(t+".*", "")+ t)*/usedPrefix.contains(pre)) {
					System.out.println("Unused :"+pre);
					List<Object> result2final = new ArrayList<>();
					result.stream().forEach(o -> {
						try {
							if (f.getType().getName().toLowerCase().contains("list")
									|| f.getType().getName().toLowerCase().contains("set")) {
								Collection<?> v = (Collection<?>) f.get(o);
								if (v != null)
									result2final.addAll(v);
							} else {
								Object oo = f.get(o);
								if (oo != null)
									result2final.add(oo);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					});
					result2_uf = result2final.parallelStream().filter(ff -> ff != null).collect(Collectors.toList());
				} else {
					//System.out.println(t+" _UsedPre:"+pre);
					usedPrefix.add(pre);
					//System.out.println(p2.toSql());
					result2_uf = (List<Object>) p2.get();
				}

				if (result2_uf == null || result2_uf.size() == 0)
					break;

				List<?> result2 = result2_uf.parallelStream().distinct().collect(Collectors.toList());
				p = p2;
				Map<String, List<Object>> maps = result.parallelStream()
						.collect(Collectors.groupingBy(d -> geneRatedHash(false, joins, d)));
				Map<String, List<Object>> maps2 = result2.parallelStream()
						.collect(Collectors.groupingBy(d -> geneRatedHash(true, joins, d)));
				
				maps.keySet().stream().forEach(b -> {
					maps.get(b).parallelStream().forEach(c -> {
						if (maps2.containsKey(b))
							try {
								if (f.getType().getName().toLowerCase().contains("list")) {
									f.set(c, maps2.get(b));
								} else {
									f.set(c, maps2.get(b).get(0));
								}
							} catch (Exception e) {
								throw new RuntimeException(e);
							}
						else {
						}
					});
				});
				result.stream().forEach(j->{
					((Model<?>)j).joined.add(f.getName());
				});
				/*
				 * result.parallelStream().forEach(o -> { try { if
				 * (f.getType().getName().toLowerCase().contains("list")) { f.set(o,
				 * result2.parallelStream().filter(h -> prediCate(o, h, joins))
				 * .collect(Collectors.toList())); } else { f.set(o,
				 * result2.parallelStream().filter(h -> prediCate(o, h, joins)).findFirst()
				 * .orElse(null)); } } catch (Exception e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); } });
				 */
				result = result2;
				result.stream().forEach(setJoined->{
					((Model<?>)setJoined).joined.add(t);
				});
				f.setAccessible(false);
				JOIN.disable(joins);
			}
		});
		lastQuerryTime = start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
		return tList;
	}
	
	public T first() {
		this.limit(0, 1);
		
		List<T> result = this.get();
		if(result.size() > 0)
			return result.get(0);
		return null;
	}

	public T get(int id) {
		return null;
	}

	public long count() {
		long result = 0;
		Predicate<T> predict = this.clone();
		try {
			predict.select = "count(*) ";
			Connection connection = manager.getConnection();
			PreparedStatement st = connection.getConnection().prepareStatement(predict.toSql());
			ResultSet rs = st.executeQuery();
			rs.next();
			result = rs.getLong(1);
			rs.close();
			st.close();
			manager.dropConnection(connection);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

	public long sum(String collumn) {
		long result = 0;
		Predicate<T> predict = this.clone();
		try {
			predict.select = "sum(collumn) ";
			Connection connection = manager.getConnection();
			PreparedStatement st = connection.getConnection().prepareStatement(predict.toSql());
			ResultSet rs = st.executeQuery();
			rs.next();
			result = rs.getLong(1);
			rs.close();
			st.close();
			manager.dropConnection(connection);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	public Predicate<T> select(String... select){
		this.select = String.join(" , ", select);
		return this;
	}

	String group;
	public Predicate<T> groupBy(String column) {
		group=column;
		return this;
	}

	public Predicate<T> where(String collumn, Object value) {
		if(value.getClass().equals(String.class))
			value = "'"+value+"'";
		if(value.getClass().isEnum()) {
			value = "'"+value.toString()+"'";
		}
		this.where += "and " + tableHeader + "." + collumn + " = " + value + " ";
		return this;
	}

	public Predicate<T> where(String collumn, String predict, Object value) {
		this.where += "and " + tableHeader + "." + collumn + " " + predict + " '" + value + "'";
		return this;
	}

	public Predicate<T> whereNull(String collumn) {
		this.where += "and " + tableHeader + "." + collumn + " is null ";
		return this;
	}

	public Predicate<T> orWhereNull(String collumn) {
		this.where += "or " + tableHeader + "." + collumn + " is null ";
		return this;
	}

	public Predicate<T> orWhereNotNull(String collumn) {
		this.where += "or " + tableHeader + "." + collumn + " is not null ";
		return this;
	}

	public Predicate<T> whereNotNull(String collumn) {
		this.where += "and " + tableHeader + "." + collumn + " is not null ";
		return this;
	}

	public Predicate<T> where(PredicateBuilder predict) {
		predict.where = predict.where.replaceAll(""+predict.tableHeader+"", "e");
		this.where += "and ( " + predict.clearWhere() + " )";
		return this;
	}

	public Predicate<T> whereIn(String collumn, Object[] value) {
		this.where += "and " + tableHeader + "." + collumn + " in ("
				+ String.join(",",
						Arrays.asList(value).parallelStream().map(a -> "'" + a + "'").collect(Collectors.toList()))
				+ ")";
		return this;
	}

	public Predicate<T> whereIn(String collumn, Stream<?> value) {
		this.where += "and " + tableHeader + "." + collumn + " in ("
				+ String.join(",", value.map(a -> "'" + a + "'").collect(Collectors.toList())) 
				+ ")";
		return this;
	}
	
	public Predicate<T> whereNotIn(String collumn, Object[] value) {
		this.where += "and " + tableHeader + "." + collumn + " not in ("
				+ String.join(",",
						Arrays.asList(value).parallelStream().map(a -> "'" + a + "'").collect(Collectors.toList()))
				+ ")";
		return this;
	}

	public Predicate<T> whereBetween(String collumn, LocalDateTime start, LocalDateTime end) {
		this.where += "and " + tableHeader + "." + collumn + " between '"
				+ start.format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")) + "' AND '"
				+ end.format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss")) + "' ";
		return this;
	}

	public void chunk(int size, Method m) {
		m.setAccessible(true);
		Predicate<T> predict = (Predicate<T>) this.clone();
		if (m.getParameterCount() != 1 || !m.getParameterTypes()[0].equals(List.class))
			throw new RuntimeException("Method error");
		int start = 0;
		while (true) {
			List<T> result = predict.limit(start, size).get();
			try {
				m.invoke(null, result);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (result.size() != size) {
				break;
			}
			start += size;
		}
		m.setAccessible(false);
	}

	public void chunk(int size, Consumer<List<T>> m) {
		int start = 0;
		Predicate<T> predict = (Predicate<T>) this.clone();
		while (true) {
			List<T> result = predict.limit(start, size).get();
			try {
				m.accept(result);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (result.size() != size) {
				break;
			}
			start += size;
		}
	}

	
	
	private Predicate(String select, String where, String tableHeader, Map<String, List<Field>> fieldMap,
			Class<T> clazz, String named, ConnectionManager manager, String group,
			Map<String, PredicateBuilder> withList, String limit, String orderBy) {
		super();
		this.select = select;
		this.where = where;
		this.tableHeader = tableHeader;
		this.fieldMap = fieldMap;
		this.clazz = clazz;
		this.named = named;
		this.manager = manager;
		this.group = group;
		this.withList = withList;
		this.limit = limit;
		this.orderBy = orderBy;
	}

	@Override
	protected Predicate<T> clone() {
		return new Predicate<T>(select, where, tableHeader, fieldMap, clazz, named, manager, group, withList, limit, orderBy);
	}
	
	public Chunk<T> getChunk(int size) {
		return new Chunk<T>(this, size);
	}

	public Predicate<T> whereHas(String field, PredicateBuilder predict) {
		try {

			Field f = this.clazz.getDeclaredField(field);
			if (predict.tableHeader.equals("e")) {
				predict.predicateBuilderSetField(f);
			}
			List<JOIN> joins = getJoins(f, this.clazz);

			String joined = "";
			for (JOIN j : joins) {
				joined += " and " + this.tableHeader + "." + j.base.getName() + " = " + f.getName() + "."
						+ j.target.getName() + " ";
			}
			this.where += "and exists ( " + predict.toSql() + joined + " )";
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return this;
	}
	
	public Predicate<T> whereHasNot(String field, PredicateBuilder predict) {
		try {

			Field f = this.clazz.getDeclaredField(field);
			if (predict.tableHeader.equals("e")) {
				predict.predicateBuilderSetField(f);
			}
			List<JOIN> joins = getJoins(f, this.clazz);

			String joined = "";
			for (JOIN j : joins) {
				joined += " and " + this.tableHeader + "." + j.base.getName() + " = " + f.getName() + "."
						+ j.target.getName() + " ";
			}
			this.where += "and not exists ( " + predict.toSql() + joined + " )";
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return this;
	}

	public Predicate<T> orWhere(String collumn, Object value) {
		this.where += "or " + tableHeader + "." + collumn + " = '" + value + "'";
		return this;
	}

	public Predicate<T> orWhere(String collumn, String predict, Object value) {
		this.where += "or " + tableHeader + "." + collumn + " " + predict + " '" + value + "'";
		return this;
	}

	public Predicate<T> orWhere(PredicateBuilder predict) {
		predict.where = predict.where.replaceAll(""+predict.tableHeader+"", "e");
		this.where += "or ( " + predict.clearWhere() + " )";
		return this;
	}

	Map<String, PredicateBuilder> withList = new HashMap<String, PredicateBuilder>();

	public Predicate<T> with(String field) {
		this.withList.put(field, null);
		return this;
	}

	public Predicate<T> with(String field, PredicateBuilder where) {

		this.withList.put(field, where);
		return this;
	}

	private String clearWhere() {
		String clear = "";
		if (this.where.startsWith("or "))
			clear = where.replaceFirst("or ", "");
		else if (this.where.startsWith("and "))
			clear = where.replaceFirst("and ", "");
		else
			clear = where;
		return clear;
	}

	public String toSql() {
		Annotations.Table t = this.clazz.getDeclaredAnnotation(Annotations.Table.class);
		String myWhere = "";

		if (clearWhere().length() > 0) {
			myWhere = "WHERE " + clearWhere() + "\n";
		}
		return "SELECT "+select+" "
			 + "FROM " + t.name() + 
			 " e\n" + myWhere +"\n"+
			( (group != null && group.length()>0) ?" group by "+group+"\n":"" )
			 +"\n"
			 +orderBy+"\n" + limit;

	}

	protected static List<JOIN> getJoins(Field f, Class<?> base) {
		Class<?> target = f.getType();
		try {
			List<JOIN> joins = new ArrayList<>();
			if (target.getName().toLowerCase().contains("list") || target.getName().toLowerCase().contains("set")) {
				ParameterizedType pType = (ParameterizedType) f.getGenericType();
				target = (Class<?>) pType.getActualTypeArguments()[0];
			}
			if (f.getAnnotation(Join.class) != null) {
				Join j = f.getAnnotation(Join.class);
				joins.add(
						new JOIN(target.getDeclaredField(j.target_collumn()), base.getDeclaredField(j.base_collumn())));
			}
			if (f.getAnnotation(Joins.class) != null)
				for (Join j : ((Joins) f.getAnnotation(Joins.class)).JOINS())
					joins.add(new JOIN(target.getDeclaredField(j.target_collumn()),
							base.getDeclaredField(j.base_collumn())));

			return joins;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	String limit = "";

	public Predicate<T> limit(int start, int size) {
		limit = "limit " + start + " , " + size;
		return this;
	}
	
	String orderBy = "";

	public Predicate<T> orderBy(String ordered , String type) {
		orderBy = "order by  " + ordered + " " + type;
		return this;
	}

	/*private boolean prediCate(Object e, Object o, List<JOIN> joins) {
		for (JOIN join : joins) {
			try {
				Field f1 = join.base;
				Field f2 = join.target;

				if (!f1.get(e).equals(f2.get(o))) {
					return false;
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}
		}
		return true;
	}*/

	private String geneRatedHash(boolean targer, List<JOIN> joins, Object o) {

		String hashKay = "";

		try {
			if (targer) {
				for (JOIN join : joins) {
					Object object = join.target.get(o);
					if (object != null)
						hashKay += object;
				}
			} else {
				for (JOIN join : joins) {
					Object object = join.base.get(o);
					if (object != null)
						hashKay += object;
				}
			}
			return DigestUtils.md5Hex(hashKay);
		} catch (Exception e) {
			throw new RuntimeException("HasGenerator faild");
		}
	}

	protected static class JOIN {
		Field target;
		Field base;

		public JOIN(Field target, Field base) {
			super();
			this.target = target;
			this.base = base;
		}

		public Field getTarget() {
			return target;
		}

		public void setTarget(Field target) {
			this.target = target;
		}

		public Field getBase() {
			return base;
		}

		public void setBase(Field base) {
			this.base = base;
		}

		public static void enable(List<JOIN> joins) {
			joins.parallelStream().forEach(a -> {
				a.target.setAccessible(true);
				a.base.setAccessible(true);
			});
		}

		public static void disable(List<JOIN> joins) {
			joins.parallelStream().forEach(a -> {
				a.target.setAccessible(false);
				a.base.setAccessible(false);
			});
		}

	}
}
