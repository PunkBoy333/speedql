package SpeedQl.Engine;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import SpeedQl.Engine.Predicate.JOIN;

public class PredicateBuilder {

	Class<?> clazz;
	String where = "";
	String tableHeader;
	Map<String, List<Field>> fieldMap;

	public PredicateBuilder(Field clazz) {
		Class<?> type = clazz.getType();
		if (type.getName().toLowerCase().contains("list") || type.getName().toLowerCase().contains("set")) {
			ParameterizedType pType = (ParameterizedType) clazz.getGenericType();
			type = (Class<?>) pType.getActualTypeArguments()[0];
		}
		this.clazz = type;
		tableHeader = "`"+clazz.getName()+"`";
		fieldMap = new HashMap<String, List<Field>>();
		fieldMap.put(clazz.getName(), new ArrayList<Field>(Arrays.asList(this.clazz.getDeclaredFields())));
		fieldMap.get(clazz.getName())
				.addAll(new ArrayList<Field>(Arrays.asList(this.clazz.getSuperclass().getDeclaredFields())));

	}

	protected void predicateBuilderSetField(Field f) {
		this.clazz = f.getType();
		this.where = this.where.replaceAll(" " + tableHeader + "\\.", " " + f.getName() + ".");
		this.tableHeader = "`"+f.getName()+"`";
		fieldMap = new HashMap<String, List<Field>>();
		fieldMap.put(clazz.getName(), new ArrayList<Field>(Arrays.asList(this.clazz.getDeclaredFields())));
		fieldMap.get(clazz.getName())
				.addAll(new ArrayList<Field>(Arrays.asList(this.clazz.getSuperclass().getDeclaredFields())));

	}

	public PredicateBuilder() {
		tableHeader = "e";
		/*
		 * tableHeader =clazz.getName(); fieldMap = new HashMap<String, List<Field>>();
		 * fieldMap.put(clazz.getName(), new
		 * ArrayList<Field>(Arrays.asList(this.clazz.getDeclaredFields())));
		 * fieldMap.put(clazz.getName(), new
		 * ArrayList<Field>(Arrays.asList(this.clazz.getFields())));
		 */
	}

	public PredicateBuilder where(String collumn, Object value) {
		this.where += "and " + tableHeader + "." + collumn + " = '" + value + "'";
		return this;
	}

	public PredicateBuilder where(String collumn, String predict, Object value) {
		this.where += "and " + tableHeader + "." + collumn + " " + predict + " '" + value + "'";
		return this;
	}

	public PredicateBuilder where(PredicateBuilder predict) {
		predict.where = predict.where.replaceAll(""+predict.tableHeader+"", "e");
		this.where += "and ( " + predict.clearWhere() + " )";
		return this;
	}

	public PredicateBuilder whereIn(String collumn, Object[] value) {
		this.where += "and " + tableHeader + "." + collumn + " in ("
				+ String.join(",",
						Arrays.asList(value).parallelStream().map(a -> "'" + a + "'").collect(Collectors.toList()))
				+ ")";
		return this;
	}
	
	public PredicateBuilder whereBetween(String column , Object start , Object end) {
		this.where += "and  "+column + " between "+ "'"+start.toString()+"' AND '"+end.toString()+"'";
		return this;
	}

	public PredicateBuilder whereHas(String field, PredicateBuilder predict) {
		try {

			Field f = this.clazz.getDeclaredField(field);
			if (predict.tableHeader.equals("e")) {
				predict.predicateBuilderSetField(f);
			}
			List<JOIN> joins = Predicate.getJoins(f, this.clazz);

			String joined = "";
			for (JOIN j : joins) {
				joined += " and " + this.tableHeader + "." + j.base.getName() + " = " + f.getName() + "."
						+ j.target.getName() + " ";
			}
			this.where += "and exists ( " + predict.toSql() + joined + " )";
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return this;
	}
	
	public PredicateBuilder orWhereHas(String field, PredicateBuilder predict) {
		try {
			System.out.println(this.clazz);
			Field f = this.clazz.getDeclaredField(field);
			if (predict.tableHeader.equals("e")) {
				predict.predicateBuilderSetField(f);
			}
			List<JOIN> joins = Predicate.getJoins(f, this.clazz);

			String joined = "";
			for (JOIN j : joins) {
				joined += " and " + this.tableHeader + "." + j.base.getName() + " = " + f.getName() + "."
						+ j.target.getName() + " ";
			}
			this.where += "or exists ( " + predict.toSql() + joined + " )";
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return this;
	}

	public PredicateBuilder orWhere(String collumn, Object value) {
		this.where += "or " + tableHeader + "." + collumn + " = '" + value + "'";
		return this;
	}

	public PredicateBuilder orWhere(String collumn, String predict, Object value) {
		this.where += "or " + tableHeader + "." + collumn + " " + predict + " '" + value + "'";
		return this;
	}

	public PredicateBuilder orWhere(PredicateBuilder predict) {
		predict.where = predict.where.replaceAll(""+predict.tableHeader+"", "e");
		this.where += "or ( " + predict.clearWhere() + " )";
		return this;
	}

	public PredicateBuilder whereNull(String collumn) {
		this.where += "and " + tableHeader + "." + collumn + " is null ";
		return this;
	}
	
	public PredicateBuilder orWhereNull(String collumn) {
		this.where += "or " + tableHeader + "." + collumn + " is null ";
		return this;
	}

	public PredicateBuilder whereNotNull(String collumn) {
		this.where += "and " + tableHeader + "." + collumn + " is not null ";
		return this;
	}

	public PredicateBuilder orWhereNotNull(String collumn) {
		this.where += "or " + tableHeader + "." + collumn + " is not null ";
		return this;
	}

	protected String clearWhere() {
		String clear = "";
		if (this.where.startsWith("or "))
			clear = where.replaceFirst("or ", "");
		else if (this.where.startsWith("and "))
			clear = where.replaceFirst("and ", "");
		else
			clear = where;
		return clear;
	}

	protected String toSql() {
		Annotations.Table t = this.clazz.getDeclaredAnnotation(Annotations.Table.class);
		String myWhere = "";

		if (clearWhere().length() > 0) {
			myWhere = "WHERE " + clearWhere() + "\n";
		}
		return "SELECT * " + "FROM " + t.name() + " " + tableHeader + "\n" + myWhere;

	}
}
