package SpeedQl.Engine;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.codec.digest.DigestUtils;

import SpeedQl.Engine.Annotations.Join;
import SpeedQl.Engine.Annotations.Joins;
import SpeedQl.Engine.Annotations.Table;

public class QuerryBuilder<T> {

	private Class<T> type;
	private String connection_name;

	String generated_table_name;

	String origin_table_name;

	List<Querry> where = new ArrayList<Querry>();
	Map<String, Order> orderBy;
	String groupBy;
	String having;

	String selectRaw = "*";
	String limit = "";

	Map<Field, QuerryBuilder<?>> widths = new HashMap<Field, QuerryBuilder<?>>();

	public boolean soft_deleted = true;

	public QuerryBuilder(Class<T> clazz) {
		type = clazz;
		soft_deleted = getFields(clazz).parallelStream().filter(a-> a.getName().equals("deleted_at")).count()>0;
		
		this.generated_table_name = clazz.getSimpleName();
	}

	@SuppressWarnings("unchecked")
	private QuerryBuilder(Class<?> clazz, String generated_table_name) {
		type = (Class<T>) clazz;
		soft_deleted = getFields(clazz).parallelStream().filter(a-> a.getName().equals("deleted_at")).count()>0;
		this.generated_table_name = generated_table_name;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public QuerryBuilder(QuerryBuilder<?> builder) {
		this.type = (Class<T>) builder.type;
		this.connection_name = builder.connection_name;
		this.generated_table_name = builder.generated_table_name;
		this.origin_table_name = builder.origin_table_name;
		this.where = builder.where == null ?  null :new ArrayList<>(builder.where);
		this.orderBy = builder.orderBy == null ? null : new HashMap<>(builder.orderBy);
		this.groupBy = builder.groupBy;
		this.having = builder.having;
		this.selectRaw = builder.selectRaw;
		this.limit = builder.limit;
		this.widths =builder.widths == null ? null :new HashMap<>(builder.widths);
		this.soft_deleted = builder.soft_deleted;
	}

	protected QuerryBuilder<T> setConnetcionName(String name) {
		connection_name = name;
		return this;
	}

	public QuerryBuilder<T> select(String... s) {
		selectRaw = String.join(",", Arrays.asList(s));
		return this;
	}

	public QuerryBuilder<T> orderBy(Map<String, Order> orderBy) {
		this.orderBy = orderBy;
		return this;
	}
	
	public QuerryBuilder<T> orderBy(String name , Order orderBy) {
		if(this.orderBy == null)
			this.orderBy = new HashMap<String, QuerryBuilder.Order>();
		this.orderBy.put(name, orderBy);
		return this;
	}

	public QuerryBuilder<T> groupBy(String... group_by) {
		this.groupBy = group_by == null ? null
				: String.join(",", Arrays.asList(group_by).parallelStream().collect(Collectors.toList()));
		return this;
	}

	public QuerryBuilder<T> with(String field_name, Function<QuerryBuilder<T>, QuerryBuilder<T>> function) {
		try {
			Field f = type.getDeclaredField(field_name);
			Class<?> t = getOriginType(f);
			QuerryBuilder<T> querry = new QuerryBuilder<>(t, field_name);
			if(function != null)
				function.apply(querry);
			if (this.widths.containsKey(f)) {
				this.widths.get(f).where.addAll(querry.where);
			} else {
				this.widths.put(f, querry);
			}

			return this;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
	
	public QuerryBuilder<T> limit(int start , int size) {
		limit = "limit " + start + " , " + size;
		return this;
	}

	public QuerryBuilder<T> bracket(Function<QuerryBuilder<T>, QuerryBuilder<T>> function) {
		QuerryBuilder<T> querry = new QuerryBuilder<T>(type, generated_table_name);
		function.apply(querry);
		this.where.add(new Querry(querry, true));
		return this;
	}

	public QuerryBuilder<T> orBracket(Function<QuerryBuilder<T>, QuerryBuilder<T>> function) {
		QuerryBuilder<T> querry = new QuerryBuilder<T>(type, generated_table_name);
		function.apply(querry);
		this.where.add(new Querry(querry, true));
		return this;
	}

	public QuerryBuilder<T> has(String column, Function<QuerryBuilder<T>, QuerryBuilder<T>> function)
			throws Exception {
		Field f = type.getDeclaredField(column);
		Class<?> t =getOriginType(f);
		QuerryBuilder<T> querry = new QuerryBuilder<>(t, generated_table_name + "_" + column);
		if (function != null)
			querry.bracket(function);
		if (f.getDeclaredAnnotation(Join.class) != null) {
			Join j = f.getDeclaredAnnotation(Join.class);
			querry.where.add(new Querry(true, j.target_collumn(), "=",
					"`" + generated_table_name + "`.`" + j.base_collumn() + "`", false));
		} else if (f.getDeclaredAnnotation(Joins.class) != null) {
			Joins joins = f.getDeclaredAnnotation(Joins.class);
			for (Join j : joins.JOINS())
				querry.where.add(new Querry(true, j.target_collumn(), "=",
						"`" + generated_table_name + "`.`" + j.base_collumn() + "`", false));
		} else
			throw new RuntimeException("relation not found JOIN");

		this.where.add(new Querry(querry, column, true));

		return this;
	}

	public QuerryBuilder<T> hasNot(String column, Function<QuerryBuilder<T>, QuerryBuilder<T>> function)
			throws Exception {
		Field f = type.getDeclaredField(column);
		Class<?> t = getOriginType(f);
		QuerryBuilder<T> querry = new QuerryBuilder<>(t, generated_table_name + "_" + column);
		if (function != null)
			querry.bracket(function);
		if (f.getDeclaredAnnotation(Join.class) != null) {
			Join j = f.getDeclaredAnnotation(Join.class);
			querry.where.add(new Querry(true, j.target_collumn(), "=",
					"`" + generated_table_name + "`.`" + j.base_collumn() + "`", false));
		} else if (f.getDeclaredAnnotation(Joins.class) != null) {
			Joins joins = f.getDeclaredAnnotation(Joins.class);
			for (Join j : joins.JOINS())
				querry.where.add(new Querry(true, j.target_collumn(), "=",
						"`" + generated_table_name + "`.`" + j.base_collumn() + "`", false));
		} else
			throw new RuntimeException("relation not found JOIN");

		this.where.add(new Querry(querry, column, "not exists", true));

		return this;
	}

	// WHRE
	public QuerryBuilder<T> where(String column, String eq, Object value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(true, column, eq, value));
		return this;
	}

	public QuerryBuilder<T> where(String column, Object value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(true, column, "=", value));
		return this;
	}

	public QuerryBuilder<T> where(String column, Object[] value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(true, column, "in", value));
		return this;
	}
	
	public QuerryBuilder<T> where(String column, List<Object> value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		Object [] objects = new Object[value.size()];
		for(int i = 0 ; i < value.size() ; i++)
			objects[i]=value.get(i);
		this.where.add(new Querry(true, column, "in", objects));
		return this;
	}

	public QuerryBuilder<T> whereNot(String column, Object value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(true, column, "!=", value));
		return this;
	}

	public QuerryBuilder<T> whereNot(String column, Object[] value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(true, column, "not in", value));
		return this;
	}

	public QuerryBuilder<T> whereNull(String column) {
		this.where.add(new Querry(true, column, "is null", null));
		return this;
	}

	public QuerryBuilder<T> whereNotNull(String column) {
		this.where.add(new Querry(true, column, "is not null", null));
		return this;
	}
	
	public QuerryBuilder<T> whereBetween(String column , LocalDateTime start , LocalDateTime end) {
		this.where.add(new Querry(true, column, "between", "'"+start.toString().replaceFirst("T", " ")+"' AND '" + end.toString().replaceFirst("T", " ")+"'" , false ));
		return this;
	}

	// OR WHRE
	public QuerryBuilder<T> orWhere(String column, String eq, Object value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(false, column, "eq", value));
		return this;
	}

	public QuerryBuilder<T> orWhere(String column, Object value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(false, column, "=", value));
		return this;
	}

	public QuerryBuilder<T> orWhere(String column, Object[] value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(false, column, "in", value));
		return this;
	}

	public QuerryBuilder<T> orWhereNot(String column, Object value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(false, column, "!=", value));
		return this;
	}

	public QuerryBuilder<T> orWhereNot(String column, Object[] value) {
		if (value == null)
			throw new RuntimeException("value is null using whereNull or whereNotNull");
		this.where.add(new Querry(false, column, "not in", value));
		return this;
	}

	public QuerryBuilder<T> orWhereNull(String column) {
		this.where.add(new Querry(false, column, "is null", null));
		return this;
	}

	public QuerryBuilder<T> orWhereNotNull(String column) {
		this.where.add(new Querry(false, column, "is not null", null));
		return this;
	}
	
	public QuerryBuilder<T> orWhereBetween(String column , LocalDateTime start , LocalDateTime end) {
		this.where.add(new Querry(false, column, "between", "'"+start.toString().replaceFirst("T", " ")+"' AND '" + end.toString().replaceFirst("T", " ")+"'" , false));
		return this;
	}

	private static class Querry {

		boolean and;
		String column;
		String operator;
		String value;
		boolean is_apos;

		QuerryBuilder<?> builder;
		List<Querry> querries;

		public Querry(boolean and, String column, String operator, Object value) {
			super();
			this.is_apos = value != null && (value instanceof String);
			this.and = and;
			this.column = column;
			this.operator = operator;
			this.value = is_apos ? "'" + value + "'" : value + "";
		}

		public Querry(boolean and, String column, String operator, Object value, boolean forceApos) {
			super();
			this.is_apos = value != null && (value instanceof String);
			this.and = and;
			this.column = column;
			this.operator = operator;
			this.value = forceApos ? "'" + value + "'" : value + "";
		}

		public Querry(boolean and, String column, String operator, Object[] values) {
			super();
			this.is_apos = values != null && (values instanceof String[]);
			this.and = and;
			this.column = column;
			this.operator = operator;
			if(values == null)
				this.value = null;
			else 
				this.value = is_apos
						? "(" + String.join(",",
								Arrays.asList(values).parallelStream().map(a -> "'" + a + "'").collect(Collectors.toList()))
								+ ")"
						: "(" + String.join(",",
								Arrays.asList(values).parallelStream().map(a -> a + "").collect(Collectors.toList())) + ")";
		}

		public Querry(QuerryBuilder<?> builder, boolean and) {
			this.and = and;
			this.builder = builder;
			querries = builder.where;
		}

		public Querry(QuerryBuilder<?> builder, String column, boolean and) {
			this.and = and;
			this.builder = builder;
			querries = builder.where;
			this.column = column;
		}

		public Querry(QuerryBuilder<?> builder, String column, String operator, boolean and) {
			this.and = and;
			this.builder = builder;
			this.operator = operator;
			querries = builder.where;
			this.column = column;
		}

	}

	public static enum Order {
		asc, desc
	}
	public boolean findDeleted() {
		return findDeleted(null);
	}
	
	public boolean findDeleted(List<Querry> querry) {
		if(!soft_deleted)
			return true;
		if(querry == null)
			querry = this.where;
		for(Querry q : querry) {
			if (q.querries == null) {
				if(q.column.equals("deleted_at"))
					return true;
			} else if(findDeleted(q.querries))
				return true;
		}
		return false;
	}

	private String whereBuilder() {
		
		if(!findDeleted())
			this.whereNull("deleted_at");
		
		String where = "";
		for (Querry q : this.where) {
			if (q.querries == null) {
				if (where.length() > 0) {
					if (q.and)
						where += " and ";
					else
						where += " or ";
				}
				where += "`" + this.generated_table_name + "`.`" + q.column + "` " + q.operator + " "
						+ (q.value != null ? q.value : "");
			} else {
				if (q.column == null) {
					QuerryBuilder<?> builder = q.builder;
					builder.soft_deleted = false;
					builder.where = q.querries;
					if (where.length() > 0) {
						if (q.and)
							where += " and ";
						else
							where += " or ";
					}
					where += builder.whereBuilder();
				} else {
					try {
						QuerryBuilder<?> builder = q.builder;
						builder.where = q.querries;
						if (where.length() > 0) {
							if (q.and)
								where += " and ";
							else
								where += " or ";
						}
						where += " " + (q.operator == null ? "exists" : q.operator) + " (\n\t"
								+ builder.build().replaceAll("\n", "\n\t") + ")";
					} catch (Exception e) {
						new RuntimeException("relations not found");
					}

				}
			}

		}
		return " (" + where + ") ";
	}

	public String build() {
		String toSql = "";

		toSql = "SELECT " + selectRaw + "\n" + "FROM " + this.type.getAnnotation(Table.class).name() + " as "
				+ this.generated_table_name + "\n" + "WHERE " + whereBuilder();
		if (groupBy != null)
			toSql += "\nGROUP BY " + groupBy;
		if (orderBy != null) {
			toSql += "\nORDER BY " + String.join(",", orderBy.keySet().stream()
					.map(a -> a + " " + orderBy.get(a).toString()).collect(Collectors.toList()));
		}
		toSql+="\n\n"+limit;

		return toSql;
	}
	
	
	public static List<Field> getFields(Class<?> clazz)
	{
		List<Field> fields =new ArrayList<Field>();
		if(clazz.getSuperclass()!= null )
			fields.addAll(getFields(clazz.getSuperclass()));
		fields.addAll(Arrays.asList(clazz.getDeclaredFields())); 
		
		return fields;
	}

	@SuppressWarnings("unchecked")
	public List<T> get() {
		ConnectionManager manager = ConnectionManager.getNamedConnection(connection_name);
		
		List<T> tList = new ArrayList<T>();
		List<Field> argumentList = QuerryBuilder.getFields(type);
		Connection con = manager.getConnection();
		try {
			T clone = (T) type.getConstructor().newInstance();

			java.sql.CallableStatement st = con.connection
					.prepareCall(this.build());
			ResultSet rs = st.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			List<String> collumns = new ArrayList<String>();
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				collumns.add(rsmd.getColumnName(i));
			}
			// System.out.println("Exec query::" + ChronoUnit.SECONDS.between(start,
			// LocalDateTime.now()));
			while (rs.next()) {
				T o = ((Model<T>) clone).clone();
				T origin = ((Model<T>) clone).clone();
				argumentList.stream().forEach(f-> {
						try {
							TypeCast.setCastValue(f, o, rs);
							f.setAccessible(true);
							f.set(origin, f.get(o));
						}catch(Exception e) {
							
						}
					}
				);
				((Model<T>) o).origin = origin;
				((Model<T>) o).collumns = collumns;
				tList.add(o);
			}
			// System.out.println("Build list::" + ChronoUnit.SECONDS.between(start,
			// LocalDateTime.now()));
			st.close();
			rs.close();
		} catch (Exception e) {
			// System.out.println(this.toSql());
			throw new RuntimeException(e);
		}
		manager.dropConnection(con);
		if (tList == null || tList.size() < 1)
			return tList;
		
		this.widths.keySet().parallelStream()
			.forEach(with->{
				
				with.setAccessible(true);
				Join[] joins;
				if(with.getDeclaredAnnotation(Join.class) != null)
					joins = new Join[] {with.getDeclaredAnnotation(Join.class)};
				else if(with.getDeclaredAnnotation(Joins.class) != null)
					joins = with.getDeclaredAnnotation(Joins.class).JOINS();
				else
					throw new RuntimeException("relation table not found "+with.getName());
				QuerryBuilder<?> builder = new QuerryBuilder<>(this.widths.get(with));
				for(Join j : joins) {
					try {
						Field base = this.type.getDeclaredField(j.base_collumn());
						base.setAccessible(true);
						builder.where(j.target_collumn(), tList.parallelStream().map(m-> {
							try {
								return base.get(m)+"";
							} catch (IllegalArgumentException | IllegalAccessException e) {
								throw new RuntimeException(e);
							}
						}).collect(Collectors.toList()));
					}catch (Exception e) {
						throw new RuntimeException(e);
					}
					
				}
				System.out.println(builder.build());
				List<?> sublist = builder.get();
				//Xcombinate
				if(sublist != null && sublist.size() > 0)
				tList.parallelStream().forEach(a->{
					String hash = geneRatedHash(false, joins, a);
					List<?> filtered = sublist.parallelStream().filter(b-> geneRatedHash(true, joins,b).equals(hash)).collect(Collectors.toList());
					if(filtered.size() > 0) {
						try {
							if (with.getType().getName().toLowerCase().contains("list")) {
								with.set(a, filtered);
							} else {
								with.set(a, filtered.get(0));
							}
						}catch (Exception e) {
							throw new RuntimeException(e);
						}
						
					}
					
				});
				
			});

		
		return tList;
	}
	
	public T first() {
		this.limit(0,1);
		List<T> result = this.get();
		if(result == null || result.size() == 0)
			return null;
		return result.get(0);
	}
	
	public long count() {
		ConnectionManager manager = ConnectionManager.getNamedConnection(connection_name);
		long result = 0;
		QuerryBuilder<T> predict = new QuerryBuilder<>(this.type);
		predict.where = this.where;
		try {
			predict.selectRaw = "count(*) ";
			Connection connection = manager.getConnection();
			PreparedStatement st = connection.getConnection().prepareStatement(predict.build());
			ResultSet rs = st.executeQuery();
			rs.next();
			result = rs.getLong(1);
			rs.close();
			st.close();
			manager.dropConnection(connection);
		}catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	
	private String geneRatedHash(boolean targer, Join[] joins, Object o) {

		String hashKay = "";

		try {
			if (targer) {
				for (Join join : joins) {
					Field f = o.getClass().getDeclaredField(join.target_collumn());
					f.setAccessible(true);
					Object object = f.get(o);
					if (object != null)
						hashKay += object;
				}
			} else {
				for (Join join : joins) {
					Field f = o.getClass().getDeclaredField(join.base_collumn());
					f.setAccessible(true);
					Object object = f.get(o);
					if (object != null)
						hashKay += object;
				}
			}
			return DigestUtils.md5Hex(hashKay);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("HasGenerator faild");
		}
	}
	
	
	private Class<?> getOriginType(Field f)
	{
		Class<?> type = f.getType();
		if (type.getName().toLowerCase().contains("list") || type.getName().toLowerCase().contains("set")) {
			ParameterizedType pType = (ParameterizedType) f.getGenericType();
			type = (Class<?>) pType.getActualTypeArguments()[0];
		}
		return type;
	}

}
