package SpeedQl.Engine;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

public class TypeCast {

	public static void setCastValue(Field f, Object o, ResultSet value) throws RuntimeException {
		try {
			Class<?> type = f.getType();
			@SuppressWarnings("deprecation")
			boolean ac = f.isAccessible();
			f.setAccessible(true);
			try {
				if (value.getObject(f.getName()) == null) {
					f.set(o, null);
					return;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

			// integers type
			if (Arrays.asList(new Class<?>[] { int.class, Integer.class }).contains(type)) {
				setInt(f, o, value);
			} else if (Arrays.asList(new Class<?>[] { long.class, Long.class }).contains(type)) {
				setLong(f, o, value);
			}
			// real type
			else if (Arrays.asList(new Class<?>[] { double.class, Double.class }).contains(type)) {
				setDouble(f, o, value);
			} else if (Arrays.asList(new Class<?>[] { String.class }).contains(type)) {
				setString(f, o, value);
			} else if (Arrays.asList(new Class<?>[] { LocalDate.class }).contains(type)) {
				setLocalDate(f, o, value);
			} else if (Arrays.asList(new Class<?>[] { LocalDateTime.class }).contains(type)) {
				setLocalDateTime(f, o, value);
			} else if (Arrays.asList(new Class<?>[] { boolean.class, Boolean.class }).contains(type)) {
				setBool(f, o, value);
			} else if (type.isEnum()) {
				setEnum(f, o, value);
			}

			f.setAccessible(ac);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void setCastValue(String head, Field f, Object o, ResultSet value) throws RuntimeException {
		try {
			Class<?> type = f.getType();
			@SuppressWarnings("deprecation")
			boolean ac = f.isAccessible();
			f.setAccessible(true);
			if (Arrays.asList(new Class<?>[] { int.class, Integer.class }).contains(type)) {
				setInt(f, o, value, head);
			} else if (Arrays.asList(new Class<?>[] { long.class, Long.class }).contains(type)) {
				setLong(f, o, value);
			} else if (Arrays.asList(new Class<?>[] { double.class, Double.class }).contains(type)) {
				setDouble(f, o, value, head);
			} else if (Arrays.asList(new Class<?>[] { String.class }).contains(type)) {
				setString(f, o, value, head);
			} else if (Arrays.asList(new Class<?>[] { LocalDate.class }).contains(type)) {
				setLocalDate(f, o, value, head);
			} else if (Arrays.asList(new Class<?>[] { LocalDateTime.class }).contains(type)) {
				setLocalDateTime(f, o, value, head);
			} else if (Arrays.asList(new Class<?>[] { boolean.class, Boolean.class }).contains(type)) {
				setBool(f, o, value, head);
			} else if (type.isEnum()) {
				setEnum(f, o, value, head);
			}
			f.setAccessible(ac);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void setInt(Field f, Object o, ResultSet value, String... head) throws Exception {
		f.set(o, value.getInt(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName()));
	}

	private static void setLong(Field f, Object o, ResultSet value, String... head) throws Exception {
		f.set(o, value.getLong(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName()));
	}

	private static void setDouble(Field f, Object o, ResultSet value, String... head) throws Exception {
		f.set(o, value.getDouble(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName()));
	}

	private static void setString(Field f, Object o, ResultSet value, String... head) throws Exception {
		f.set(o, value.getString(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName()));
	}

	private static void setLocalDate(Field f, Object o, ResultSet value, String... head) throws Exception {
		f.set(o, LocalDate
				.parse(value.getString(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName())));
	}

	private static void setLocalDateTime(Field f, Object o, ResultSet value, String... head) throws Exception {
		Timestamp tsm = value.getTimestamp(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName());
		f.set(o, tsm == null ? null : tsm.toLocalDateTime());
	}

	private static void setBool(Field f, Object o, ResultSet value, String... head) throws Exception {
		f.set(o, value.getBoolean(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName()));
	}

	private static void setEnum(Field f, Object o, ResultSet value, String... head) throws Exception {
		String values = value.getString(head != null && head.length > 0 ? head[0] + "." + f.getName() : f.getName());
		f.set(o, Arrays.asList(f.getType().getEnumConstants()).stream().filter(a -> a.toString().equals(values))
				.findFirst().orElse(null));
	}

}
